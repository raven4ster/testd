import os
from os.path import abspath
from pathlib import Path

DB_HOST = os.environ.get('DB_HOST'),
DB_NAME = os.environ.get('DB_NAME'),
DB_USER = os.environ.get('DB_USER'),
DB_PASSWORD = os.environ.get('DB_PASSWORD')

MIGRATIONS_DIR = abspath(Path(os.getcwd()) / "migrations")
