CREATE TABLE pytest.sample(id UInt32, name String) ENGINE MergeTree PARTITION BY tuple()
ORDER BY tuple()

CREATE TABLE IF NOT EXISTS payments_queue
(
    id             UInt64,
    status         String,
    cents          Int64,
    created_at     DateTime,
    payment_method String,
    version        UInt64
)
ENGINE=Kafka('clickhouse:9292', 'payments_topic', 'payments_group1, 'JSONEachRow');