# import datetime
# from kafka import KafkaProducer
# from kafka.errors import KafkaError
import json
from numpy import random
import statistics
from datetime import datetime
# from time import sleep
# from kafka import KafkaProducer

nstep = 0o1
fnum = 10
num_bid = []
num_ask = []
avr_stats = []

dt = datetime.now()
# getting the timestamp
ts = datetime.timestamp(dt)
print("Unix timestamp:", ts)


class SRV:
    def bid_func(nstep, fnum, num_bid):
        global avr_bid
        global bid_json
        while nstep <= 50:
            ranum = random.uniform(1, fnum)
            map_bid = f"bid_{nstep:d}: {ranum:.2f}"
            fnum += 10
            nstep += 1
            # print(map_bid)

            num_bid.append(ranum)
        avr_bid = statistics.fmean(num_bid)
        bid_json = json.dumps(map_bid, ensure_ascii=False).encode('utf8')
        # print('AVR_BID:',avr_bid)

    bid_func(nstep, fnum, num_bid)

    def ask_func(nstep, fnum, num_ask):
        global avr_ask
        global ask_json
        while nstep <= 50:
            ranum = random.uniform(1, fnum)
            map_ask = f"ask_{nstep:d}: {ranum:.2f}"
            fnum += 10
            nstep += 1
            # print(map_ask)

            num_ask.append(ranum)
        avr_ask = statistics.fmean(num_ask)
        print(json.dumps(map_ask, ensure_ascii=False).encode('utf8'))
        # print('AVR_ASK:', avr_ask)

    ask_func(nstep, fnum, num_ask)

stats_json = {}

stats_json['AVR_ASK'] = avr_ask

print(stats_json)

# appending the data
# ask_json.update(bid_json)
# # theresult is a JSON string:
# print(json.dumps(ask_json))
    # def avr_stats(avr_ask, avr_bid):
    #     res_json = json.dumps(avr_bid, ensure_ascii=False).encode('utf8')
    #     json_file = res_json.add(avr_ask)
    #     n = json_file.write(res_json)
    #     print("n")

    # avr_stats(avr_ask, avr_bid)
