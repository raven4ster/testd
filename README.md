# testD

## Integrate with your tools

- [x] [Set up project integrations](https://gitlab.com/raven4ster/testd/-/settings/integrations)

## Test and Deploy

Use the built-in continuous integration in GitLab.

# Editing this README

When you're ready to make this README your own, just edit this file and use the handy template below (or feel free to structure it however you want - this is just a starting point!). Thank you to [makeareadme.com](https://www.makeareadme.com/) for this template.

## Roadmap

- [ ] 1. service
- [x] 2. docker-compose
  - app
  - zookeper
  - broker(kafka)
  - clichouse
  - client-clickhouse
  - network
- [x] 2. migration-clichouse
- [ ] 3. monitoring

## Author

**# Chuvashov Mikhail Vladimirovich**

## Project status

in progress ....
